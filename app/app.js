function get_data(client, event_data){
  client.iparams.get().then (
  function(iparam) {
    var event_data_string = event_data.toString();
    if(event_data == iparam.start_status)
    {
      create_timer(client);
    }
    else if(iparam.stop_statuses.includes(event_data_string))
    {
      if(!["4","5"].includes(event_data_string)){
        toggle_timer(client);
      }
    }
  },
  function() {
    client.interface.trigger("showNotify",{type:"danger", message:"Auto-Timer app load failed! Please refresh the page to reload the app."});
  });
}

function create_timer(client){
  client.interface.trigger("start", {id: "timer"});
}

function toggle_timer(client){
  client.interface.trigger("stop", {id: "timer"});
}

$(document).ready( function() {
  app.initialized()
  .then(function(_client) {
    var client = _client;
    
    var propertyChangeCallback = function(data) 
    {
      var detail = data.data;
      if(detail.changedAttributes.status)
      {
        get_data(client, detail.changedAttributes.status[1]);
      }
    };
    client.events.on('ticket.propertiesUpdated', propertyChangeCallback);
  });
});